import React from "react";

import DefaultLayout from "./layouts/DefaultLayout";

import Home from "./components/Home";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";

import jQuery from "jquery";
import 'bootstrap/dist/css/bootstrap.min.css';

import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<DefaultLayout/>}>
          <Route index element={<Home/>} />
          <Route path="/auth/register" element={<Register />} />
          <Route path="/auth/login" element={<Login />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App;

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from 'yup';

import React, { useState } from "react";

import './login.css'
//Message
import SuccessMessage from "../items/SuccessMessage";
import ErrorMessage from "../items/ErrorMessage";
//Login component
const Login = () => {
    const [isSuccess, setIsSucess] = useState(false);
    
    //Validation schema
    const validationSchema = Yup.object().shape({
        username: Yup.string()
        .required('Username is required')
        .min(6, 'Username must be at least 6 characters')
        .max(20, 'Username must not exceed 20 characters'),
        password: Yup.string()
        .required('Password is required')
        .min(6, 'Password must be at least 6 characters')
        .max(40, 'Password must not exceed 40 characters')
    });
    
    const {
        register,
        handleSubmit,
        reset,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(validationSchema)
    });

    const onSubmit = data => {
        //Turn on the sucess status to show the message
        setIsSucess(true);
        console.log(JSON.stringify(data, null, 2));
    };

    return (
        <div className="login_form">
            <h1>Login</h1>
            <form className="mt-4" onSubmit={handleSubmit(onSubmit)}>
                <div className="mb-3">
                    <label htmlFor="username" className="form-label label-title">username</label>
                    <input type="text" name="username" className={`form-control ${errors.username ? 'is-invalid' : ''}`} id="username" aria-describedby="User name" {...register('username')}/>
                    <div className="invalid-feedback">{errors.username?.message}</div>
                </div>

                <div className="mb-3">
                    <label htmlFor="password" className="form-label label-title">password</label>
                    <input type="password" name="password" className={`form-control ${errors.password ? 'is-invalid' : ''}`} id="password" {...register('password')}/>
                    <div className="invalid-feedback">{errors.password?.message}</div>
                </div>
                {isSuccess &&
                    <SuccessMessage message="Login Successfully!"/>
                }
                <div className="mb-3">
                    <button type="submit" className="btn btn-primary m-2">Login</button>
                    <button type="button" className="btn btn-secondary m-2" onClick={reset}>Reset</button>
                </div>  
            </form>
        </div>        
    )
};

export default Login;

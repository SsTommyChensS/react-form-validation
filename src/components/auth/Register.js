import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from 'yup';

import React, { useState } from "react";

import './register.css'
//Message
import SuccessMessage from "../items/SuccessMessage";
import ErrorMessage from "../items/ErrorMessage";
//Register component
const Register = () => {
    const [isSucess, setIsSucess] = useState(false);

    //Validation schema
    const validationSchema = Yup.object().shape({
        fullname: Yup.string().required('Fullname is required'),
        username: Yup.string()
        .required('Username is required')
        .min(6, 'Username must be at least 6 characters')
        .max(20, 'Username must not exceed 20 characters'),
        email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
        password: Yup.string()
        .required('Password is required')
        .min(6, 'Password must be at least 6 characters')
        .max(40, 'Password must not exceed 40 characters'),
        confirmPassword: Yup.string()
        .required('Confirm Password is required')
        .oneOf([Yup.ref('password'), null], 'Confirm Password does not match'),
        acceptTerms: Yup.bool().oneOf([true], 'Accept Terms is required')
    });
    
    const {
        register,
        handleSubmit,
        reset,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(validationSchema)
    });

    const onSubmit = data => {
        //Turn on the sucess status to show the message
        setIsSucess(true);
        console.log(JSON.stringify(data, null, 2));
    };

    return (
        <div className="register_form">
            <h1>Register</h1>
            <form className="mt-4" onSubmit={handleSubmit(onSubmit)}>
                <div className="mb-3">
                    <label htmlFor="full_name" className="form-label label-title">full name</label>
                    <input type="text" name="fullname" className={`form-control ${errors.fullname ? 'is-invalid' : ''}`} id="full_name" aria-describedby="Full name" placeholder="Type your fullname here" {...register('fullname')}/>
                    <div className="invalid-feedback">{errors.fullname?.message}</div>
                </div>
                <div className="mb-3">
                    <label htmlFor="username" className="form-label label-title">user Name</label>
                    <input type="text" name="username" className={`form-control ${errors.username ? 'is-invalid' : ''}`} id="username" aria-describedby="User name" placeholder="Type your username here"{...register('username')}/>
                    <div className="invalid-feedback">{errors.username?.message}</div>
                </div>
                <div className="mb-3">
                    <label htmlFor="email" className="form-label label-title">email</label>
                    <input type="text" name="email" className={`form-control ${errors.email ? 'is-invalid' : ''}`} id="email" aria-describedby="Email" placeholder="Type your email here" {...register('email')}/>
                    <div className="invalid-feedback">{errors.email?.message}</div>
                </div>
                <div className="mb-3">
                    <label htmlFor="password" className="form-label label-title">password</label>
                    <input type="password" name="password" className={`form-control ${errors.password ? 'is-invalid' : ''}`} id="password" placeholder="Type your password here" {...register('password')}/>
                    <div className="invalid-feedback">{errors.password?.message}</div>
                </div>
                <div className="mb-3">
                    <label htmlFor="password_confirm" className="form-label label-title">confirm your password</label>
                    <input type="password" name="password_confirm" className={`form-control ${errors.confirmPassword ? 'is-invalid' : ''}`} id="password_confirm" placeholder="Type your confirm password here" {...register('confirmPassword')}/>
                    <div className="invalid-feedback">{errors.confirmPassword?.message}</div>
                </div>
                <div className="mb-3 form-check">
                    <input type="checkbox" className={`form-check-input ${errors.acceptTerms ? 'is-invalid' : ''}`} id="term_agree" {...register('acceptTerms')}/>
                    <label className="form-check-label label-term" htmlFor="term_agree">I have read and argeed to the Terms</label>
                    <div className="invalid-feedback">{errors.acceptTerms?.message}</div>
                </div>
                {isSucess &&
                    <SuccessMessage message="Register successfully!"/>
                }
                <div className="mb-3">
                    <button type="submit" className="btn btn-primary m-2">Submit</button>
                    <button type="button" className="btn btn-secondary m-2" onClick={reset}>Reset</button>
                </div>  
            </form>
        </div>        
    )
};

export default Register;